#include <FastLED.h>
#include <EtherCard.h>

// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include <Wire.h>
#include "RTClib.h"
RTC_DS1307 rtc;

#define LED_PIN     3
#define NUM_LEDS    115
#define BRIGHTNESS  64
#define LED_TYPE    WS2812
#define COLOR_ORDER GRB
#define TEXT_BUFFER_SIZE 50
CRGB leds[NUM_LEDS];

const String charIndex = "ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789?!:\\";
byte charArray[41][4] = {
  {0b01111001, 0b10011111, 0b10010000, 4},
  {0b11101001, 0b11101001, 0b11100000, 4},
  {0b01101001, 0b10001001, 0b01100000, 4},
  {0b11101001, 0b10011001, 0b11100000, 4},
  {0b11111000, 0b11101000, 0b11110000, 4},
  {0b11111000, 0b11101000, 0b10000000, 4},
  {0b01111000, 0b10111001, 0b01110000, 4},
  {0b10011001, 0b11111001, 0b10010000, 4},
  {0b11100100, 0b01000100, 0b11100000, 4},
  {0b01110001, 0b00011001, 0b01100000, 4},
  {0b10011010, 0b11001010, 0b10010000, 4},
  {0b10001000, 0b10001000, 0b11110000, 4},
  {0b10011111, 0b10011001, 0b10010000, 4},
  {0b10011101, 0b10111001, 0b10010000, 4},
  {0b01101001, 0b10011001, 0b01100000, 4},
  {0b11101001, 0b11101000, 0b10000000, 4},
  {0b01101001, 0b10011011, 0b01110000, 4},
  {0b11101001, 0b11101010, 0b10010000, 4},
  {0b01111000, 0b01100001, 0b11100000, 4},
  {0b11100100, 0b01000100, 0b01000000, 4},
  {0b10011001, 0b10011001, 0b01100000, 4},
  {0b10101010, 0b10101010, 0b01000000, 4},
  {0b10011001, 0b10011111, 0b10010000, 4},
  {0b10011001, 0b01101001, 0b10010000, 4},
  {0b10101010, 0b01000100, 0b01000000, 4},
  {0b11110001, 0b01101000, 0b11110000, 4},
  {0,0,0, 4},
  {0b01101011, 0b11011001, 0b01100000, 4},
  {0b01001100, 0b01000100, 0b01000000, 2},
  {0b01101001, 0b00100100, 0b11110000, 4},
  {0b11100001, 0b01100001, 0b11100000, 4},
  {0b10011001, 0b11110001, 0b00010000, 4},
  {0b11111000, 0b11100001, 0b11100000, 4},
  {0b01101000, 0b11101001, 0b01100000, 4},
  {0b11110001, 0b00100010, 0b00100000, 4},
  {0b01101001, 0b01101001, 0b01100000, 4},
  {0b01101001, 0b01110001, 0b01100000, 4},
  {0b11000010, 0b01000000, 0b01000000, 3},
  {0b10001000, 0b10000000, 0b10000000, 1},
  {0b00001000, 0b00001000, 0b00000000, 1},
  {0,0,0,1}
};

TBlendType currentBlending;
uint8_t colorIndex;
byte Ethernet::buffer[700];
static uint8_t mymac[] = { 0x32,0x29,0x55,0x98,0x14,0x65 };
char textBuffer[TEXT_BUFFER_SIZE];
long startTime = DateTime(2022, 9, 13, 15, 59, 50).unixtime();
const char tapMessage[] PROGMEM = "    HET IS 4 UUR GEWEEST DE TAP IS LOS!";

void setup() {  
    delay( 3000 ); // power-up safety delay
    if (! rtc.begin()) {
      while (1);
    }
    if (! rtc.isrunning()) {
      // following line sets the RTC to the date & time this sketch was compiled
      rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    }
    if (ether.begin(sizeof Ethernet::buffer, mymac, SS) == 0){
      Serial.println(F("Failed to access Ethernet controller"));
    }
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
    currentBlending = LINEARBLEND;
    colorIndex = 0;
}

void loop() {
  // put your main code here, to run repeatedly:
  clearText();
  DateTime date = rtc.now();
  bool scroll = printRemainingString(textBuffer, TEXT_BUFFER_SIZE, date);
  if (scroll){
    scrollText(textBuffer, 30);
    delay(2000);
  } else {
    displayString(textBuffer, 0, 0);
    FastLED.show();
    delay(30);
  }
}

void clearText(){
  for(int i = 0; i < TEXT_BUFFER_SIZE -1; i++){
    textBuffer[i] = 0;
  }
}

bool printRemainingString(char buffer[], int buffer_size, DateTime date){

  int delta = (10 - date.dayOfTheWeek()) % 7;
  if (date.hour() >= 20) {
    delta = (9 - date.dayOfTheWeek()) % 7;
  }
  int delta_hour = (43 - date.hour()) % 24;
  if (date.dayOfTheWeek() == 2 && date.hour() >= 16 && date.hour() < 19){
    strcpy_P(buffer, tapMessage);
    return true;
  }
  if (delta > 0) {
    snprintf(buffer, buffer_size, "    NOG %d DAGEN", delta + 1);
    return true;
  } else if (delta_hour > 0) {
    int delta_min = (59 - date.minute()) % 60;
    snprintf(buffer, buffer_size, "%02d:%02d", delta_hour, delta_min);
    return false;
  } else {
    int delta_min = (59 - date.minute()) % 60;
    int delta_sec = (59 - date.second()) % 60;
    snprintf(buffer, buffer_size, "%02d:%02d", delta_min, delta_sec);
    return false;
  }
}

void blockColor(CRGB color){
  for(int i =0; i<=(NUM_LEDS-1); i++){
    leds[i] = color;
  }
  FastLED.show();
}

int getLEDID(int x, int y){
  if ((x & 0x01) == 0){
    return (22-x) * 5 + y;
  } else {
    return (22-x) * 5 + (5-y) -1;
  }
}

void scrollText(char input[], double delayValue){
  for (int i =0; i < strlen(input); i++){
    for(int b =0; b < 5; b++){
      displayString(input, i, b);
      FastLED.show();
      delay(delayValue);
    }
  }
}

CRGB getColor(){
//  return ColorFromPalette( PartyColors_p, colorIndex, BRIGHTNESS, currentBlending);
  CRGB val = {255, 237, 0};
  return val;
}

CRGB getBGColor() {
  CRGB val = {60, 1, 5};
  return CRGB::Black;
}

void displayString(char value[], int startChar, double offset){
  int curCol = 0;
  colorIndex += 1;
  for (int i=startChar;i<strlen(value);i++){
    byte curChar[4];
    if (value[i] == 0){
      break;
    }
    memcpy(curChar, charArray[charIndex.indexOf(value[i])], 4);
    for(int b=0; b<curChar[3]; b++){
      if (offset != 0){
        b = offset;
        offset = 0;
        if (b >= curChar[3]){
          break;
        }
      }
      leds[getLEDID(curCol, 0)] = curChar[0] >> (7-b) & 0x01 ? getColor() : getBGColor();
      leds[getLEDID(curCol, 1)] = curChar[0] >> (3-b) & 0x01 ? getColor() : getBGColor();
      leds[getLEDID(curCol, 2)] = curChar[1] >> (7-b) & 0x01 ? getColor() : getBGColor();
      leds[getLEDID(curCol, 3)] = curChar[1] >> (3-b) & 0x01 ? getColor() : getBGColor();
      leds[getLEDID(curCol, 4)] = curChar[2] >> (7-b) & 0x01 ? getColor() : getBGColor();
      curCol += 1;
      if (curCol > 22){
        break;
      }
    }
    if (curCol < 23){
        setLedCol(curCol, getBGColor());
        curCol += 1;
    }
    if (curCol > 22){
      break;
    }
  }
  while(curCol < 23){
    setLedCol(curCol, getBGColor());
    curCol += 1;
  }
  
}

void setLedCol(int col, CRGB colour) {
  for(int i = 0; i<5; i++){
    leds[getLEDID(col, i)] = colour;
  }
}
